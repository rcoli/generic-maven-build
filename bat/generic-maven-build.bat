:: Instruções de Uso
:: Coloque o caminho dos arquivos pom.xml que deseja efetuar o build separados por espaços na ordem de precedencia.
:: Coloque por último se deseja ou não efetuar os testes, true para execução de testes e false para pulá-los, os testes
:: serão executados por default caso esse argumento seja omitido
:: Exemplo de uso:
:: generic-maven-buil.bat D:\work\alelo_dev\alelo_commons\api D:\work\alelo_dev\alelo_commons\geral D:\work\alelo_dev\alelo_meualelo\meualelo\meualelo-web D:\work\alelo_dev\alelo_admin\alelo-admin D:\work\alelo_dev\alelo_pedido\pedido true

@echo off
setlocal enabledelayedexpansion

set argCount=0
set buildsComSucesso=0

for %%x in (%*) do (
   set /A argCount+=1
   set "argVec[!argCount!]=%%~x"
   set "pularTeste=%%~x"
)
if not %pularTeste% == true set "pularTeste=false"

echo ====================================
echo Numero de argumentos recebidos: %argCount%
echo Pular os testes?! %pularTeste%
echo ====================================

for /L %%i in (1,1,%argCount%) do (
    
    if not !argVec[%%i]! == true if not !argVec[%%i]! == false (
    
        if exist !argVec[%%i]!\pom.xml (
            echo mvn -f !argVec[%%i]!\pom.xml clean install -U -DskipTests=%pularTeste%
            call mvn -f !argVec[%%i]!\pom.xml clean install -U -DskipTests=%pularTeste%
REM         echo Exit Code = %ERRORLEVEL%
            if not "%ERRORLEVEL%" == "0" (
                echo "FALHA ao executar" !argVec[%%i]!"!"
                pause 
                exit /b
            )
            echo ---> Sucesso! Build !argVec[%%i]! finalizada!
            echo --------------------------
            set /A buildsComSucesso+=1
        ) else (
            echo Arquivo nao encontrado: !argVec[%%i]!\pom.xml
            echo --------------------------
            pause 
            exit /b
        )
    )
)

echo %buildsComSucesso% projetos compilados!!!