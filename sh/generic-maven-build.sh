#!/bin/bash

# Instruções de Uso
# Coloque o caminho dos arquivos pom.xml que deseja efetuar o build separados por espaços na ordem de precedencia.
# Coloque por último se deseja ou não efetuar os testes, "semTestes" para pulá-los ou omití-la para executá-los
# Exemplo de uso:
# Sem executar os testes
# generic-maven-build.sh /home/user/repository/project1/pom.xml /home/user/repository/project1/pom.xml semTestes
# Com a execução de testes
# # generic-maven-build.sh /home/user/repository/project1/pom.xml /home/user/repository/project1/pom.xml

palavraChave='semTestes'
buildSucesso=''
buildErro=''

# caso o ultimo argumento não seja a palavra chave, executo os testes.
[[ "${!#}" = "$palavraChave" ]] && pularTestes=true || pularTestes=false

function exibirArgumentos(){
    printf '\t%s\n' "$@"
}

function validarArgumentos(){

    if [ -z "$1" ] || [ "$1" = $palavraChave ]; then
        printf '\n\n\t'
        echo "ERRO! Por favor escolha um arquivo pom.xml para compilar. Ex: D:\\Projetos\repositorio\projeto\pom.xml"
        printf '\n\n'
        exit 1
    fi

    projetosInvalidos=''

    for var in "$@"
        do
            if ! [[ -f "$var" ]]; then
                if [[ "$var" != "$palavraChave" ]]; then
                    projetosInvalidos+="$var "              
                fi
            fi
    done

    if ! [[ -z $projetosInvalidos ]]; then
        printf '\n\n\tERRO! Arquivo(s) inválido(s):\n'
        exibirArgumentos $projetosInvalidos
        printf '\n\n'
        exit 1
    fi
}

function exibirCabecalho(){
    printf '\n\n====================================\n'
    echo "===================================="
    echo " $# argumentos recebidos:"
        exibirArgumentos $@
    echo " Pular os testes?! $pularTestes"
    printf '====================================\n'
}

function exibirRodape(){
    exibirCabecalho $@
    printf "======== RESUMO DA OPERAÇÃO ========\n"
    echo " Projetos bem Sucedidos:"
    exibirArgumentos $buildSucesso
    echo " Projetos com ERRO:"
    exibirArgumentos $buildErro
    printf '====================================\n'
}

function exibirTempoDecorrido(){
    elapseTime=$(($2 - $1)) 
    printf "====================================\n"
    printf "Tempo de execução: %02dh %02dm %02ds\n" "$((elapseTime/3600%24))" "$((elapseTime/60%60))" "$((elapseTime%60))"
    printf '====================================\n\n'
}

function executarBuild(){
    echo "Comando: mvn -f $1 clean install -U -DskipTests=$pularTestes"
    printf '====================================\n'
    mvn -f $1 clean install -U -DskipTests=$pularTestes
}

function compilarProjetos(){
    for var in "$@"
        do
            if [[ "$var" != "$palavraChave" ]]; then
        
                printf '\n\n====================================\n'
                echo "Executando o build de $var"

                if executarBuild $var; then
                    buildSucesso+="$var "
                else 
                    buildErro+="$var "
                fi
            fi
    done
}

validarArgumentos $@
retornoValidacao=$?

if [[ $retornoValidacao == 0 ]]; then

    startTime=$(date +%s)

    exibirCabecalho $@

    compilarProjetos $@

    exibirRodape $@

    endTime=$(date +%s)

    exibirTempoDecorrido $startTime $endTime 
fi